#!/usr/bin/env python2
from setuptools import setup, find_packages
import os

try:
    from setuptest import test
except ImportError:
    from setuptools.command.test import test

version = __import__('sitecontact').get_version()


def read(fname):
    # read the contents of a text file
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "django-contact",
    version = version,
    url = 'https://bitbucket.org/oddotterco/django-contact',
    license = 'BSD',
    platforms=['OS Independent'],
    description = "A Django app that will create a pluggable django-cms contact form plugin within your project.",
    keywords='django, cms',
    author = "Odd Otter Co",
    author_email = 'sitecontact@oddotter.com',
    packages = find_packages(),
    install_requires = (
    	'Django>=1.4',
    	'django-cms>=2.3.5',
    ),
    include_package_data = True,
    zip_safe = False,
    classifiers = [
        'Development Status :: 2 - Pre-Alpha',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    package_dir = {
        'sitecontact': 'sitecontact',
    },
)
