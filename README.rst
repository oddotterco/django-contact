===========
django-contact
===========

A Django app that will create a pluggable django-cms contact form plugin within your project.

Dependencies
============

- django
- django-cms

Getting Started
=============

TBD

Usage
=====

TBD

Production Environment
======================

TBD
